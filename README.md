# Tokenizer

## Auth device token and edge token for advanced developers

You need a valid certificate to use this.

Copy your certificate extracted from your PRODINFO.BIN into the folder containing tokenizer.exe

Run.

Several log files will be created.

If your device is not banned you will get an edge.token file.

If you don't know what's the goal of this tool, don't use it.

By using this tool you can be banned.

You can get it [here](https://gitlab.com/retrogamer_74/tokenizer/-/releases)
